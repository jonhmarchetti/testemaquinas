Prezado Adilson, 

Algumas questões apresentaram resposta diferente da resposta em seu diretório, provavelmente a base de dados usada pelas suas respostas está mais atualizada em relação a base de dados informada no arquivo CREATE.

Segue minhas observações: 

Questão 01: De acordo com o banco de dados informado, mesmo olhando manualmente, a primeira data de entrada da máquina PPA está diferente da informada seu diretório de respostas.
Questão 04: De acordo com o banco de dados informado, mesmo checando manualmente, houve apenas duas máquinas que deram entrada ou saída no dia 01/01/2020, enquanto no diretório de respostas houveram mais máquinas. 
Questão 05: De acordo com o banco de dados informado, mesmo olhando manualmente, algumas informações não batem com a resposta informada no seu diretório, como por exemplo a entrada mais recente da máquina PPA, YYC e ZZA. 

No mais, deu para desenvolver as querys normalmente, só deixo essas observações pois talvez algum outro candidato não perceba.

Obrigado! Att. Jonathan Marchetti 