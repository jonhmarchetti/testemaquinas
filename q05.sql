SELECT l.maquina, l.latitude, l.longitude, max(h.entrada), max(h.saida) FROM hist_geo h 
INNER JOIN last_geo l ON l.maquina = h.maquina
WHERE h.saida > h.entrada
GROUP BY h.maquina;  